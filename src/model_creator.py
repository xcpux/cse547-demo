import sys
import tensorflow as tf
import tensorflowjs as tfjs
from tensorflow import keras
from subprocess import PIPE, Popen

models = [
    'VGG19',
    'ResNet50',
    'MobileNet'
]

if __name__ == "__main__":
    saved_model_dir = '../static/saved_models/'
    quantization_bytes = sys.argv[1] if len(sys.argv) > 1 else '1'
    Popen(['rm', '-rf', saved_model_dir], stdin=PIPE, bufsize=-1).communicate()
    Popen(['mkdir', saved_model_dir], stdin=PIPE, bufsize=-1).communicate()
    for model_name in models:
        tf.keras.backend.clear_session()
        model_filename = saved_model_dir + model_name
        model = getattr(keras.applications, model_name)(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
        model.save(model_filename + '.h5')
        Popen([
                'tensorflowjs_converter', 
                '--input_format=keras', 
                model_filename+'.h5',
                model_filename + '/'
            ], 
            stdin=PIPE, bufsize=-1
        ).communicate()
        Popen([
                'tensorflowjs_converter', 
                '--quantization_bytes', 
                quantization_bytes, 
                '--input_format=keras', 
                model_filename+'.h5', 
                model_filename + 
                '_QUANTIZED/'
            ], 
            stdin=PIPE, bufsize=-1
        ).communicate()
        Popen([
                'rm', 
                '-rf', 
                model_filename+'.h5',
            ], 
            stdin=PIPE, bufsize=-1
        ).communicate()
